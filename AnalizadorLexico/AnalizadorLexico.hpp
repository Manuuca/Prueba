#pragma once

#include "tipos.hpp"

// Busca en el archivo el primer componente lexico que encuentre y cambia el control a la posicion de este, el complex a su componente lexico correspondiente y cambia el lexema a la cadena literal encontrada
bool ObtenerSiguienteComplex(std::ifstream& fuente, ulong& control, AnalizadorLexico::ComponenteLexico& complex, std::string& lexema, AnalizadorLexico::TablaSimbolos& ts);